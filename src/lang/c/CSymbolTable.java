package lang.c;

import lang.SymbolTable;

public class CSymbolTable {
    private class OneSymbolTable extends SymbolTable<CSymbolTableEntry> {

        @Override
        public CSymbolTableEntry register(String name, CSymbolTableEntry e) {
            return put(name, e);
        }

        @Override
        public CSymbolTableEntry search(String name) {
            return get(name);
        }
    }

    private OneSymbolTable global;  // 大域変数用
    private OneSymbolTable local;   // 局所変数用

    public CSymbolTable() {
        global = new OneSymbolTable();
        local = new OneSymbolTable();
    }

    public boolean setGlobalSymbol(String name, CSymbolTableEntry e) {
        CSymbolTableEntry entry = getGlobalSymbol(name);
        if (entry != null) {
            return false;
        }
        global.register(name, e);
        return true;
    }

    public CSymbolTableEntry getGlobalSymbol(String name) {
        return global.search(name);
    }

    public void showGlobalSymbolTable() {
        global.show();
    }

    public boolean setLocalSymbol(String name, CSymbolTableEntry e) {
        CSymbolTableEntry entry = getLocalSymbol(name);
        if (entry != null) {
            return false;
        }
        local.register(name, e);
        return true;
    }

    public CSymbolTableEntry getLocalSymbol(String name) {
        return local.search(name);
    }

    public void showLocalSymbolTable() {
        local.show();
    }
}
