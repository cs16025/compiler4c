package lang.c;

import lang.SimpleToken;

public class CToken extends SimpleToken {
    public static final int TK_PLUS     = 2;    // +
    public static final int TK_SLASH    = 3;    // /
    public static final int TK_ASTERISK = 4;    // *
    public static final int TK_MINUS    = 5;    // -
    public static final int TK_AMP		= 6;	// &
    public static final int TK_LPAR     = 7;    // (
    public static final int TK_RPAR     = 8;    // )
    public static final int TK_LBRA		= 9;	// [
    public static final int TK_RBRA		= 10;	// ]
    public static final int TK_IDENT	= 11;	// 識別子
    public static final int TK_ASSIGN   = 12;   // =
    public static final int TK_SEMI     = 13;   // ;
    public static final int TK_COMMA    = 14;   // ,
    public static final int TK_INT      = 15;   // int
    public static final int TK_CONST    = 16;   // const
    public static final int TK_LT		= 17;	// <
    public static final int TK_LE		= 18;   // <=
    public static final int TK_GT       = 19;   // >
    public static final int TK_GE       = 20;   // >=
    public static final int TK_EQ       = 21;   // ==
    public static final int TK_NE       = 22;   // !=
    public static final int TK_TRUE     = 23;   // true
    public static final int TK_FALSE    = 24;   // false
    public static final int TK_LCUR		= 25;	// {
    public static final int TK_RCUR		= 26;	// }
    public static final int TK_IF		= 27;	// if
    public static final int TK_ELSE		= 28;	// else
    public static final int TK_WHILE    = 29;   // while
    public static final int TK_INPUT    = 30;   // input
    public static final int TK_OUTPUT   = 31;   // output


    public CToken(int type, int lineNo, int colNo, String s) {
        super(type, lineNo, colNo, s);
    }
}
