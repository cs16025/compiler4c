package lang.c;

import lang.*;
import lang.c.parse.*;

public class MiniCompiler {
	public static void main(String[] args) {
		String inFile = args[0]; // 適切なファイルを絶対パスで与えること
		IOContext ioCtx = new IOContext(inFile, System.out, System.err);
		CTokenizer tknz = new CTokenizer(new CTokenRule());
		CSymbolTable cst = new CSymbolTable();
		CParseContext pctx = new CParseContext(ioCtx, tknz, cst);
		try {
			CTokenizer ct = pctx.getTokenizer();
			CToken tk = ct.getNextToken(pctx);
			if (Program.isFirst(tk)) {
				CParseRule parseTree = new Program(pctx);
				parseTree.parse(pctx);									// 構文解析
//                cst.showGlobalSymbolTable();
				if (pctx.hasNoError()) parseTree.semanticCheck(pctx);		// 意味解析
				if (pctx.hasNoError()) parseTree.codeGen(pctx);			// コード生成
				pctx.errorReport();
			} else {
				pctx.fatalError(tk.toExplainString() + "プログラムの先頭にゴミがあります");
			}
		} catch (FatalErrorException e) {
			e.printStackTrace();
		}
	}
}
