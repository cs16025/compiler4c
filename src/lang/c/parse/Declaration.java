package lang.c.parse;

import lang.FatalErrorException;
import lang.c.CParseContext;
import lang.c.CParseRule;
import lang.c.CToken;
import lang.c.CTokenizer;

import java.io.PrintStream;

public class Declaration extends CParseRule {
    // declaration ::= intDecl | constDecl
    private CParseRule decl;

    public Declaration(CParseContext pctx) {
        decl = null;
    }

    public static boolean isFirst(CToken tk) {
        return IntDecl.isFirst(tk) || ConstDecl.isFirst(tk);
    }

    public void parse(CParseContext pctx) throws FatalErrorException {
        // ここにやってくるときは、必ずisFirst()が満たされている
        CTokenizer ct;
        CToken tk;

        ct = pctx.getTokenizer();
        tk = ct.getCurrentToken(pctx);
        if (IntDecl.isFirst(tk)) {
            decl = new IntDecl(pctx);
            decl.parse(pctx);
        } else if (ConstDecl.isFirst(tk)) {
            decl = new ConstDecl(pctx);
            decl.parse(pctx);
        }
    }

    public void semanticCheck(CParseContext pctx) {}

    public void codeGen(CParseContext pctx) throws FatalErrorException {
        PrintStream o = pctx.getIOContext().getOutStream();
        o.println(";;; declaration starts");
        if (decl != null) { decl.codeGen(pctx); }
        o.println(";;; declaration completes");
    }
}
