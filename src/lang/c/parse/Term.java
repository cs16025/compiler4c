package lang.c.parse;

import java.io.PrintStream;

import lang.*;
import lang.c.*;

public class Term extends CParseRule {
	// term ::= factor { termMult | termDiv }
	private CParseRule term;
	
	public Term(CParseContext pctx) {
	    term = null;
    }
    
	public static boolean isFirst(CToken tk) {
		return Factor.isFirst(tk);
	}
	
	public void parse(CParseContext pctx) throws FatalErrorException {
		// ここにやってくるときは、必ずisFirst()が満たされている
        CParseRule factor;
        CTokenizer ct;
        CToken tk;
        CParseRule list;
        
		factor = new Factor(pctx);
		factor.parse(pctx);
		
		ct = pctx.getTokenizer();
		tk = ct.getCurrentToken(pctx);

		while (TermMult.isFirst(tk) || TermDiv.isFirst(tk)) {
		    if (TermMult.isFirst(tk)) {
		        list =  new TermMult(pctx, factor);
		        list.parse(pctx);
		        factor = list;
		        tk = ct.getCurrentToken(pctx);
            } else if (TermDiv.isFirst(tk)) {
                list =  new TermDiv(pctx, factor);
                list.parse(pctx);
                factor = list;
                tk = ct.getCurrentToken(pctx);
            }
        }
        term = factor;
	}

	public void semanticCheck(CParseContext pctx) throws FatalErrorException {
		if (term != null) {
			term.semanticCheck(pctx);
			this.setCType(term.getCType());		// term の型をそのままコピー
			this.setConstant(term.isConstant());
		}
	}

	public void codeGen(CParseContext pctx) throws FatalErrorException {
		PrintStream o = pctx.getIOContext().getOutStream();
		o.println(";;; term starts");
		if (term != null) { term.codeGen(pctx); }
		o.println(";;; term completes");
	}
}
