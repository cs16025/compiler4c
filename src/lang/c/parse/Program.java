package lang.c.parse;

import java.io.PrintStream;
import java.util.ArrayList;

import lang.*;
import lang.c.*;

public class Program extends CParseRule {
    // program ::= { declaration } { statement } EOF
    private ArrayList<CParseRule> decl, state;

    public Program(CParseContext pctx) {
        decl = new ArrayList<>();
        state = new ArrayList<>();
    }

    public static boolean isFirst(CToken tk) {
        return Declaration.isFirst(tk) || Statement.isFirst(tk);
    }

    public void parse(CParseContext pctx) throws FatalErrorException {
        // ここにやってくるときは、必ずisFirst()が満たされている
        CTokenizer ct;
        CToken tk;

        ct = pctx.getTokenizer();
        tk = ct.getCurrentToken(pctx);
        while (Declaration.isFirst(tk) || Statement.isFirst(tk)) {
            CParseRule program;
            if (Declaration.isFirst(tk)) {
                program = new Declaration(pctx);
                program.parse(pctx);
                decl.add(program);
                tk = ct.getCurrentToken(pctx);
            } else if (Statement.isFirst(tk)) {
                program = new Statement(pctx);
                program.parse(pctx);
                state.add(program);
                tk = ct.getCurrentToken(pctx);
            }
        }
        if (tk.getType() != CToken.TK_EOF) {
            pctx.fatalError(tk.toExplainString() + "プログラムの最後にゴミがあります");
        }
    }

    public void semanticCheck(CParseContext pctx) throws FatalErrorException {
        for (CParseRule program : state) {
            program.semanticCheck(pctx);
        }
    }

    public void codeGen(CParseContext pctx) throws FatalErrorException {
        PrintStream o = pctx.getIOContext().getOutStream();
        o.println(";;; program starts");
        o.println("\t. = 0x100");
        o.println("\tJMP\t__START\t; ProgramNode: 最初の実行文へ");
        for (CParseRule program : decl) {
            program.codeGen(pctx);
        }
        o.println("__START:");
        o.println("\tMOV\t#0x1000, R6\t; ProgramNode: 計算用スタック初期化");
        for (CParseRule program : state) {
            program.codeGen(pctx);
        }
        o.println("\tHLT\t\t\t; ProgramNode:");
        o.println("\t.END\t\t\t; ProgramNode:");
        o.println(";;; program completes");
    }
}
