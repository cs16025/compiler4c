package lang.c.parse;

import lang.FatalErrorException;
import lang.c.CParseContext;
import lang.c.CParseRule;
import lang.c.CToken;
import lang.c.CTokenizer;

import java.io.PrintStream;
import java.util.ArrayList;

public class IntDecl extends CParseRule {
    // intDecl ::= INT declItem { COMMA declItem } SEMI
    private CToken intIdent;
    private ArrayList<CParseRule> declItems;

    public IntDecl(CParseContext pctx) {
        intIdent = null;
        declItems = new ArrayList<>();
    }

    public static boolean isFirst(CToken tk) {
        return tk.getType() == CToken.TK_INT;
    }

    public void parse(CParseContext pctx) throws FatalErrorException {
        // ここにやってくるときは、必ずisFirst()が満たされている
        CTokenizer ct;
        CToken tk;
        CParseRule declItem;

        ct = pctx.getTokenizer();
        intIdent = ct.getCurrentToken(pctx);
//        System.out.println(tk.getText());
        tk = ct.getNextToken(pctx);
        if (DeclItem.isFirst(tk)) {
            declItem = new DeclItem(pctx);
            declItem.parse(pctx);
            declItems.add(declItem);
            tk = ct.getCurrentToken(pctx);
        } else {
            pctx.fatalError(tk.toExplainString() + "intの後はdeclItemです");
        }
        while (tk.getType() == CToken.TK_COMMA) {
            tk = ct.getNextToken(pctx);
            if (DeclItem.isFirst(tk)) {
                declItem = new DeclItem(pctx);
                declItem.parse(pctx);
                declItems.add(declItem);
                tk = ct.getCurrentToken(pctx);
            } else {
                pctx.fatalError(tk.toExplainString() + "commaの後はdeclItemです");
            }
        }
        if (tk.getType() != CToken.TK_SEMI) {
            pctx.fatalError(tk.toExplainString() + "行の最後にゴミがあります");
        }
        tk = ct.getNextToken(pctx);
    }

    public void semanticCheck(CParseContext pctx) {}

    public void codeGen(CParseContext pctx) throws FatalErrorException {
        PrintStream o = pctx.getIOContext().getOutStream();
        o.println(";;; intDecl starts");
        for (CParseRule declItem : declItems) {
            declItem.codeGen(pctx);
        }
        o.println(";;; intDecl completes");
    }
}
