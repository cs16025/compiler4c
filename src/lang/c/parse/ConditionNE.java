package lang.c.parse;

import lang.FatalErrorException;
import lang.c.*;

import java.io.PrintStream;

public class ConditionNE extends CParseRule {
    // conditionNE ::= NE expression
    CToken NE;
    CParseRule left;
    CParseRule right;

    public ConditionNE(CParseContext pctx, CParseRule left) {
        NE = null;
        this.left = left;
        right = null;
    }

    public static boolean isFirst(CToken tk) {
        return tk.getType() == CToken.TK_NE;
    }

    @Override
    public void parse(CParseContext pctx) throws FatalErrorException {
        // ここにやってくるときは、必ずisFirst()が満たされている
        CTokenizer ct;
        CToken tk;

        ct = pctx.getTokenizer();
        NE = ct.getCurrentToken(pctx);
        // !=の次の字句を読む
        tk = ct.getNextToken(pctx);
        if (Expression.isFirst(tk)) {
            right = new Expression(pctx);
            right.parse(pctx);
        } else {
            pctx.fatalError(tk.toExplainString() + "!=の後ろはexpressionです");
        }}

    @Override
    public void semanticCheck(CParseContext pctx) throws FatalErrorException {
        if (left != null && right != null) {
            left.semanticCheck(pctx);
            right.semanticCheck(pctx);
            if (!left.getCType().equals(right.getCType())) {
                pctx.fatalError(NE.toExplainString() + "左辺の型[" + left.getCType().toString() + "]と右辺の型[" + right.getCType().toString() + "]が一致しないので比較できません");
            } else {
                this.setCType(CType.getCType(CType.T_bool));
                this.setConstant(true);
            }
        }
    }

    @Override
    public void codeGen(CParseContext pctx) throws FatalErrorException {
        PrintStream o = pctx.getIOContext().getOutStream();
        o.println(";;; condition != (compare) starts");
        if (left != null && right != null) {
            left.codeGen(pctx);
            right.codeGen(pctx);
            int seq = pctx.getSeqId();
            o.println("\tMOV\t-(R6), R0\t; ConditionNE: ２数を取り出して、比べる");
            o.println("\tMOV\t-(R6), R1\t; ConditionNE:");
            o.println("\tCLR\tR2\t\t; ConditionNE: set false");
            o.println("\tCMP\tR0, R1\t; ConditionNE: R1!=R0 = R1-R0!=0");
            o.println("\tBRZ\tNE" + seq + " ; ConditionNE:");
            o.println("\tMOV\t#0x0001, R2\t; ConditionNE: set true");
            o.println("NE" + seq + ":\tMOV\tR2, (R6)+\t; ConditionNE:");
        }
        o.println(";;;condition < (compare) completes");
    }
}
