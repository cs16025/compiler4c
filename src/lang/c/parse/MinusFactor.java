package lang.c.parse;

import lang.FatalErrorException;
import lang.c.*;

import java.io.PrintStream;

public class MinusFactor extends CParseRule {
    //minusFactor ::= MINUS unsignedFactor
    private CToken minus;
    private CParseRule unsignedFactor;

    public MinusFactor(CParseContext pctx) {
        minus = null;
        unsignedFactor = null;
    }

    public static boolean isFirst(CToken tk) {
        return tk.getType() == CToken.TK_MINUS;
    }

    public void parse(CParseContext pctx) throws FatalErrorException {
        // ここにやってくるときは、必ずisFirst()が満たされている
        CTokenizer ct;
        CToken tk;

        ct = pctx.getTokenizer();
        minus = ct.getCurrentToken(pctx);
        // -の次の字句を読む
        tk = ct.getNextToken(pctx);
        if (UnsignedFactor.isFirst(tk)) {
            unsignedFactor = new UnsignedFactor(pctx);
            unsignedFactor.parse(pctx);
        } else {
            pctx.fatalError(tk.toExplainString() + "-の後ろはunsignedFactorです");
        }
    }

    public void semanticCheck(CParseContext pctx) throws FatalErrorException {
        if (unsignedFactor != null) {
            unsignedFactor.semanticCheck(pctx);
            if (unsignedFactor.getCType().getType() == CType.T_pint) {
                pctx.fatalError(minus.toExplainString() + unsignedFactor.getCType().toString() + "は負の値にできません");
            } else {
                setCType(unsignedFactor.getCType());		// number の型をそのままコピー
                setConstant(unsignedFactor.isConstant());	// number は常に定数
            }
        }
    }

    public void codeGen(CParseContext pctx) throws FatalErrorException {
        PrintStream o = pctx.getIOContext().getOutStream();
        if (unsignedFactor != null) {
            unsignedFactor.codeGen(pctx);
        }
        o.println("\tMOV\t-(R6), R0\t; MinusFactor: 数を取り出して、0から引き、積む<" + minus.toExplainString() + ">");
        o.println("\tMOV\t#0, R1\t; MinusFactor:");
        o.println("\tSUB\tR0, R1\t; MinusFactor:");
        o.println("\tMOV\tR1, (R6)+\t; MinusFactor:");
    }
}
