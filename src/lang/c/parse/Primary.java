package lang.c.parse;

import lang.FatalErrorException;
import lang.c.CParseContext;
import lang.c.CParseRule;
import lang.c.CToken;
import lang.c.CTokenizer;

import java.io.PrintStream;

public class Primary extends CParseRule {
    //prmary ::= primaryMult | valiable
    private CParseRule primary;

    public Primary(CParseContext pctx) {
        primary = null;
    }

    public static boolean isFirst(CToken tk) {
        return PrimaryMult.isFirst(tk) || Variable.isFirst(tk);
    }

    public void parse(CParseContext pctx) throws FatalErrorException {
        // ここにやってくるときは、必ずisFirst()が満たされている
        CTokenizer ct;
        CToken tk;

        ct = pctx.getTokenizer();
        tk = ct.getCurrentToken(pctx);
        if (PrimaryMult.isFirst(tk)) {
            primary = new PrimaryMult(pctx);
            primary.parse(pctx);
        } else if (Variable.isFirst(tk)) {
            primary = new Variable(pctx);
            primary.parse(pctx);
        }
    }

    public void semanticCheck(CParseContext pctx) throws FatalErrorException {
        if (primary != null) {
            primary.semanticCheck(pctx);
            setCType(primary.getCType());
            setConstant(primary.isConstant());
        }
    }

    public void codeGen(CParseContext pctx) throws FatalErrorException {
        PrintStream o = pctx.getIOContext().getOutStream();
        o.println(";;; Primary starts");
        if (primary != null) {
            primary.codeGen(pctx);
        }
        o.println(";;; Primary completes");
    }

    public boolean isMult(CParseContext pctx) {
        return primary instanceof PrimaryMult;
    }
}
