package lang.c.parse;

import lang.FatalErrorException;
import lang.c.*;

import java.io.PrintStream;

public class TermMult extends CParseRule {
    //termMult ::= MULT factor
    private CToken mult;
    private CParseRule left, right;

    public TermMult(CParseContext pctx, CParseRule left) {
        mult = null;
        this.left = left;
        right = null;
    }

    public static boolean isFirst(CToken tk) {
        return tk.getType() == CToken.TK_ASTERISK;
    }

    public void parse(CParseContext pctx) throws FatalErrorException {
        // ここにやってくるときは、必ずisFirst()が満たされている
        CTokenizer ct;
        CToken tk;

        ct = pctx.getTokenizer();
        mult = ct.getCurrentToken(pctx);
        // *の次の字句を読む
        tk = ct.getNextToken(pctx);
        if (Factor.isFirst(tk)) {
            right = new Factor(pctx);
            right.parse(pctx);
        } else {
            pctx.fatalError(tk.toExplainString() + "*の後ろはfactorです");
        }
    }

    public void semanticCheck(CParseContext pctx) throws FatalErrorException {
        //掛け算の型計算規則
        final int s[][] = {
                //		T_err			T_int          T_pint       T_int_array     T_pint_array
                {	CType.T_err,	CType.T_err,    CType.T_err,    CType.T_err,    CType.T_err},	// T_err
                {	CType.T_err,	CType.T_int,    CType.T_err,    CType.T_err,    CType.T_err},	// T_int
                {   CType.T_err,    CType.T_err,    CType.T_err,    CType.T_err,    CType.T_err},   //T_pint
                {   CType.T_err,    CType.T_err,    CType.T_err,    CType.T_err,    CType.T_err},   //T_int_array
                {   CType.T_err,    CType.T_err,    CType.T_err,    CType.T_err,    CType.T_err},   //T_pint_array
        };
        int lt = 0;
        boolean lc = false;
        int rt = 0;
        boolean rc = false;
        int nt;

        if (left != null) {
            left.semanticCheck(pctx);
            lt = left.getCType().getType();     // *の左辺の型
            lc = left.isConstant();
        } else {
            pctx.fatalError(mult.toExplainString() + "左辺がありません");
        }

        if (right != null) {
            right.semanticCheck(pctx);
            rt = right.getCType().getType();	// *の右辺の型
            rc = right.isConstant();
        } else {
            pctx.fatalError(mult.toExplainString() + "右辺がありません");
        }

        nt = s[lt][rt];
        if (nt == CType.T_err) {
            pctx.fatalError(mult.toExplainString() + "左辺の型[" + left.getCType().toString() + "]と右辺の型[" + right.getCType().toString() + "]はかけられません");
        }
        this.setCType(CType.getCType(nt));
        this.setConstant(lc && rc);				// *の左右両方が定数のときだけ定数
    }

    public void codeGen(CParseContext pctx) throws FatalErrorException {
        PrintStream o = pctx.getIOContext().getOutStream();
        if ((left != null) && (right != null)) {
            left.codeGen(pctx);
            right.codeGen(pctx);
            o.println("\tJSR\tMUL\t; TermMult: 掛け算のサブルーチンに移動<" + mult.toExplainString() + ">");
            o.println("\tSUB\t#2, R6\t; TermMult");
            o.println("\tMOV\tR0, (R6)+\t; TermMult");

        }
    }
}
