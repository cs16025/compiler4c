package lang.c.parse;

import lang.FatalErrorException;
import lang.c.*;

import java.io.PrintStream;

public class AddressToValue extends CParseRule {
    //addressToValue ::= primary
    private CParseRule primary;

    public AddressToValue(CParseContext pctx) {
        primary = null;
    }

    public static boolean isFirst(CToken tk) {
        return Primary.isFirst(tk);
    }

    public void parse(CParseContext pctx) throws FatalErrorException {
        // ここにやってくるときは、必ずisFirst()が満たされている
        primary = new Primary(pctx);
        primary.parse(pctx);
    }

    public void semanticCheck(CParseContext pctx) throws FatalErrorException {
        if (primary != null) {
            primary.semanticCheck(pctx);
            setCType(primary.getCType());
            setConstant(primary.isConstant());
        }
    }

    public void codeGen(CParseContext pctx) throws FatalErrorException {
        PrintStream o = pctx.getIOContext().getOutStream();
        o.println(";;; addressToValue starts");
        if (primary != null) {
            primary.codeGen(pctx);
            o.println("\tMOV\t-(R6), R0\t; AddressToValue: アドレスを取り出して、内容を参照して、積む<>");
            o.println("\tMOV\t(R0), (R6)+\t; AddressToValue:");
        }
        o.println(";;; addressToValue completes");
    }
}
