package lang.c.parse;

import lang.FatalErrorException;
import lang.c.*;

import java.io.PrintStream;

public class Variable extends CParseRule {
    //variable ::= ident [ array ]
    private CParseRule ident, array;

    public Variable(CParseContext pctx) {
        ident = null;
        array = null;
    }

    public static boolean isFirst(CToken tk) {
        return Ident.isFirst(tk);
    }

    public void parse(CParseContext pctx) throws FatalErrorException {
        // ここにやってくるときは、必ずisFirst()が満たされている
        CTokenizer ct;
        CToken tk;

        ident = new Ident(pctx);
        ident.parse(pctx);

        ct = pctx.getTokenizer();
        tk = ct.getCurrentToken(pctx);
        if (Array.isFirst(tk)) {
            array = new Array(pctx);
            array.parse(pctx);
        }
    }

    public void semanticCheck(CParseContext pctx) throws FatalErrorException {
        CTokenizer ct;
        CToken tk;

        if (ident != null && array != null) {
            ident.semanticCheck(pctx);
            array.semanticCheck(pctx);
            ct = pctx.getTokenizer();
            tk = ct.getCurrentToken(pctx);
            if (ident.getCType() == CType.getCType(CType.T_array)) {
                setCType(CType.getCType(CType.T_int));
                setConstant(ident.isConstant());
            } else if (ident.getCType() == CType.getCType(CType.T_parray)) {
                setCType(CType.getCType(CType.T_pint));
                setConstant(ident.isConstant());
            } else {
                pctx.fatalError(tk.toExplainString() + "ident[]は" + CType.getCType(CType.T_array) + "型または" + CType.getCType(CType.T_parray) + "型です");
            }
        } else if (ident != null && array == null) {
            ident.semanticCheck(pctx);
            ct = pctx.getTokenizer();
            tk = ct.getCurrentToken(pctx);
            if (ident.getCType() == CType.getCType(CType.T_int) || ident.getCType() == CType.getCType(CType.T_pint)) {
                setCType(ident.getCType());
                setConstant(ident.isConstant());
            } else {
                pctx.fatalError(tk.toExplainString() + "identは" + CType.getCType(CType.T_int) + "型または" + CType.getCType(CType.T_pint) + "型です");
            }
        }
    }

    public void codeGen(CParseContext pctx) throws FatalErrorException {
        PrintStream o = pctx.getIOContext().getOutStream();
        o.println(";;; variable starts");
        if (ident != null) {
            ident.codeGen(pctx);
        }
        if (array != null) {
            array.codeGen(pctx);
        }
        o.println(";;; variable completes");
    }
}
