package lang.c.parse;

import lang.FatalErrorException;
import lang.c.CParseContext;
import lang.c.CParseRule;
import lang.c.CToken;
import lang.c.CTokenizer;

import java.io.PrintStream;
import java.util.ArrayList;

public class ConstDecl extends CParseRule {
    // constDecl ::= CONST INT comstItem { COMMA constItem } SEMI
    private CToken constIdent, intIdent;
    private ArrayList<CParseRule> constItems;

    public ConstDecl(CParseContext pctx) {
        constIdent = null;
        intIdent = null;
        constItems = new ArrayList<>();
    }

    public static boolean isFirst(CToken tk) {
        return tk.getType() == CToken.TK_CONST;
    }

    public void parse(CParseContext pctx) throws FatalErrorException {
        // ここにやってくるときは、必ずisFirst()が満たされている
        CTokenizer ct;
        CToken tk;
        CParseRule constItem;

        ct = pctx.getTokenizer();
        constIdent = ct.getCurrentToken(pctx);
        tk = ct.getNextToken(pctx);
        if (tk.getType() == CToken.TK_INT) {
            intIdent = tk;
        } else {
            pctx.fatalError(tk.toExplainString() + "constの後はintです");
        }
        tk = ct.getNextToken(pctx);
        if (ConstItem.isFirst(tk)) {
            constItem = new ConstItem(pctx);
            constItem.parse(pctx);
            constItems.add(constItem);
            tk = ct.getCurrentToken(pctx);
        } else {
            pctx.fatalError(tk.toExplainString() + "intの後はconstItemです");
        }
        while (tk.getType() == CToken.TK_COMMA) {
            tk = ct.getNextToken(pctx);
            if (ConstItem.isFirst(tk)) {
                constItem = new ConstItem(pctx);
                constItem.parse(pctx);
                constItems.add(constItem);
                tk = ct.getCurrentToken(pctx);
            } else {
                pctx.fatalError(tk.toExplainString() + "commaの後はconstItemです");
            }
        }
        if (tk.getType() != CToken.TK_SEMI) {
            pctx.fatalError(tk.toExplainString() + "行の最後はsemiです");
        }
        tk = ct.getNextToken(pctx);
    }

    public void semanticCheck(CParseContext pctx) {}

    public void codeGen(CParseContext pctx) throws FatalErrorException {
        PrintStream o = pctx.getIOContext().getOutStream();
        o.println(";;; constDecl starts");
        for (CParseRule constItem : constItems) {
            constItem.codeGen(pctx);
        }
        o.println(";;; constDecl completes");
    }
}
