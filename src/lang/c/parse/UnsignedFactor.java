package lang.c.parse;

import lang.FatalErrorException;
import lang.c.CParseContext;
import lang.c.CParseRule;
import lang.c.CToken;
import lang.c.CTokenizer;

import java.io.PrintStream;

public class UnsignedFactor extends CParseRule {
    //unsignedFactor ::= factorAmp | number | LPAR expression RPAR | addressToValue
    private CParseRule number;
    private CToken lpar, rpar;

    public UnsignedFactor(CParseContext pctx) {
        number = null;
        lpar = null;
        rpar = null;
    }

    public static boolean isFirst(CToken tk) {
        return FactorAmp.isFirst(tk) || Number.isFirst(tk) || tk.getType() == CToken.TK_LPAR || AddressToValue.isFirst(tk);
    }

    public void parse(CParseContext pctx) throws FatalErrorException {
        // ここにやってくるときは、必ずisFirst()が満たされている
        CTokenizer ct;
        CToken tk;

        ct = pctx.getTokenizer();
        tk = ct.getCurrentToken(pctx);
        if (FactorAmp.isFirst(tk)) {
            number = new FactorAmp(pctx);
            number.parse(pctx);
        } else if (Number.isFirst(tk)) {
            number = new Number(pctx);
            number.parse(pctx);
        } else if (tk.getType() == CToken.TK_LPAR) {
            lpar = tk;
            tk = ct.getNextToken(pctx);
            if (Expression.isFirst(tk)) {
                number = new Expression(pctx);
                number.parse(pctx);
                tk = ct.getCurrentToken(pctx);
                if (tk.getType() == CToken.TK_RPAR) {
                    rpar = tk;
                    tk = ct.getNextToken(pctx);
                } else {
                    pctx.fatalError(tk.toExplainString() + "括弧が閉じていません");
                }
            } else {
                pctx.fatalError(tk.toExplainString() + "(の後ろはexpressionです");
            }
        } else if (AddressToValue.isFirst(tk)) {
            number = new AddressToValue(pctx);
            number.parse(pctx);
        }
    }

    public void semanticCheck(CParseContext pctx) throws FatalErrorException {
        if (number != null) {
            number.semanticCheck(pctx);
            setCType(number.getCType());		// number の型をそのままコピー
            setConstant(number.isConstant());	// number は常に定数
        }
    }

    public void codeGen(CParseContext pctx) throws FatalErrorException {
        PrintStream o = pctx.getIOContext().getOutStream();
        o.println(";;; unsignedFactor starts");
        if (lpar != null) {
        }
        if (number != null) { number.codeGen(pctx); }
        if (rpar != null) {
        }
        o.println(";;; unsignedFactor completes");
    }
}
