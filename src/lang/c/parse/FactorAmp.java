package lang.c.parse;

import lang.FatalErrorException;
import lang.c.*;

import java.io.PrintStream;

public class FactorAmp extends CParseRule {
    //factorAmp ::= AMP ( number | primary )
    private CToken amp;
    private CParseRule number;

    public FactorAmp(CParseContext pctx) {
    }

    public static boolean isFirst(CToken tk) {
        return tk.getType() == CToken.TK_AMP;
    }

    public void parse(CParseContext pctx) throws FatalErrorException {
        // ここにやってくるときは、必ずisFirst()が満たされている
        CTokenizer ct;
        CToken tk;

        ct = pctx.getTokenizer();
        amp = ct.getCurrentToken(pctx);
        tk = ct.getNextToken(pctx);
        if (Number.isFirst(tk)) {
            number = new Number(pctx);
            number.parse(pctx);
        } else if (Primary.isFirst(tk)) {
            number = new Primary(pctx);
            number.parse(pctx);
        } else {
            pctx.fatalError(tk.toExplainString() + "&の後ろはnumberかprimaryです");
        }
    }

    public void semanticCheck(CParseContext pctx) throws FatalErrorException {
        CTokenizer ct;
        CToken tk;

        if (number != null) {
            number.semanticCheck(pctx);
            ct = pctx.getTokenizer();
            tk = ct.getCurrentToken(pctx);
            if (number instanceof Primary) {
                if (!((Primary) number).isMult(pctx)) {
                    setCType(CType.getCType(CType.T_pint));
                    setConstant(number.isConstant());
                } else {
                    pctx.fatalError(tk.toExplainString() + "ポインタのアドレスはありません");
                }
            } else {
                setCType(CType.getCType(CType.T_pint));
                setConstant(number.isConstant());
            }
        }
    }

    public void codeGen(CParseContext pctx) throws FatalErrorException {
        PrintStream o = pctx.getIOContext().getOutStream();
        o.println(";;; factorAmp starts");
        if (number != null) {
            number.codeGen(pctx);
        }
        o.println(";;; factorAmp completes");
    }
}
