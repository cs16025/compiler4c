package lang.c.parse;

import lang.FatalErrorException;
import lang.c.*;

import java.io.PrintStream;

public class Ident extends CParseRule {
    //ident ::= IDENT
    private CToken ident;
    private CSymbolTableEntry gste;

    public Ident(CParseContext pctx) {
        ident = null;
        gste = null;
    }

    public static boolean isFirst(CToken tk) {
        return tk.getType() == CToken.TK_IDENT;
    }

    public void parse(CParseContext pctx) throws FatalErrorException {
        // ここにやってくるときは、必ずisFirst()が満たされている
        CTokenizer ct;
        CToken tk;

        ct = pctx.getTokenizer();
        ident = ct.getCurrentToken(pctx);
        gste = pctx.getCSymbolTable().getGlobalSymbol(ident.getText());
        if (gste == null) {
            pctx.fatalError(ident.toExplainString() + "変数の未定意義使用は禁止です");
        }
        tk = ct.getNextToken(pctx);
    }

    public void semanticCheck(CParseContext pctx) {
        if (ident != null) {
            this.setCType(gste.getType());
            this.setConstant(gste.getConstp());
        }
    }

    public void codeGen(CParseContext pctx) {
        PrintStream o = pctx.getIOContext().getOutStream();
        o.println(";;; ident starts");
        if (ident != null) {
            o.println("\tMOV\t#" + ident.getText() + ", (R6)+\t; Ident: 変数アドレスを積む<" + ident.toExplainString() + ">");
        }
        o.println(";;; ident completes");
    }
}
