package lang.c.parse;

import lang.FatalErrorException;
import lang.c.CParseContext;
import lang.c.CParseRule;
import lang.c.CToken;
import lang.c.CTokenizer;

import java.io.PrintStream;

public class PlusFactor extends CParseRule {
    //plusFactor ::= PLUS unsignedFactor
    private CToken plus;
    private CParseRule unsignedFactor;

    public PlusFactor(CParseContext pctx) {
        plus = null;
        unsignedFactor = null;
    }

    public static boolean isFirst(CToken tk) {
        return tk.getType() == CToken.TK_PLUS;
    }

    public void parse(CParseContext pctx) throws FatalErrorException {
        // ここにやってくるときは、必ずisFirst()が満たされている
        CTokenizer ct;
        CToken tk;

        ct = pctx.getTokenizer();
        plus = ct.getCurrentToken(pctx);
        // +の次の字句を読む
        tk = ct.getNextToken(pctx);
        if (UnsignedFactor.isFirst(tk)) {
            unsignedFactor = new UnsignedFactor(pctx);
            unsignedFactor.parse(pctx);
        } else {
            pctx.fatalError(tk.toExplainString() + "+の後ろはunsignedFactorです");
        }
    }

    public void semanticCheck(CParseContext pctx) throws FatalErrorException {
        if (unsignedFactor != null) {
            unsignedFactor.semanticCheck(pctx);
            setCType(unsignedFactor.getCType());		// number の型をそのままコピー
            setConstant(unsignedFactor.isConstant());	// number は常に定数
        }
    }

    public void codeGen(CParseContext pctx) throws FatalErrorException {
        PrintStream o = pctx.getIOContext().getOutStream();
        if (unsignedFactor != null) {
            unsignedFactor.codeGen(pctx);
        }
    }
}
