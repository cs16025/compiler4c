package lang.c.parse;

import java.io.PrintStream;

import lang.*;
import lang.c.*;

public class Number extends CParseRule {
	// number ::= NUM
	private CToken num;

	public Number(CParseContext pctx) {}

	public static boolean isFirst(CToken tk) {
		return tk.getType() == CToken.TK_NUM;
	}

	public void parse(CParseContext pctx) throws FatalErrorException {
		CTokenizer ct = pctx.getTokenizer();
		CToken tk = ct.getCurrentToken(pctx);
		num = tk;
		tk = ct.getNextToken(pctx);
	}

	public void semanticCheck(CParseContext pctx) throws FatalErrorException {
		this.setCType(CType.getCType(CType.T_int));
		this.setConstant(true);
	}

	public void codeGen(CParseContext pctx) throws FatalErrorException {
		PrintStream o = pctx.getIOContext().getOutStream();
		o.println(";;; number starts");
		if (num != null) {
			o.println("\tMOV\t#" + num.getText() + ", (R6)+\t; Number: 数を積む<" + num.toExplainString() + ">");
		}
		o.println(";;; number completes");
	}
}
