package lang.c.parse;

import java.io.PrintStream;

import com.sun.org.apache.xpath.internal.operations.Plus;
import lang.*;
import lang.c.*;

public class Factor extends CParseRule {
	// factor ::= plusFactor | minusFactor | unsignedFactor
	private CParseRule factor;

	public Factor(CParseContext pctx) {
	    factor = null;
    }

	public static boolean isFirst(CToken tk) {
		return PlusFactor.isFirst(tk) || MinusFactor.isFirst(tk) || UnsignedFactor.isFirst(tk);
	}

	public void parse(CParseContext pctx) throws FatalErrorException {
		// ここにやってくるときは、必ずisFirst()が満たされている
		CTokenizer ct;
		CToken tk;

		ct = pctx.getTokenizer();
		tk = ct.getCurrentToken(pctx);
		if (PlusFactor.isFirst(tk)) {
			factor = new PlusFactor(pctx);
			factor.parse(pctx);
		} else if (MinusFactor.isFirst(tk)) {
		    factor = new MinusFactor(pctx);
		    factor.parse(pctx);
		} else if (UnsignedFactor.isFirst(tk)) {
		    factor = new UnsignedFactor(pctx);
		    factor.parse(pctx);
        }
	}

	public void semanticCheck(CParseContext pctx) throws FatalErrorException {
		if (factor != null) {
			factor.semanticCheck(pctx);
			this.setCType(factor.getCType());		// factorの型をそのままコピー
			this.setConstant(factor.isConstant());
		}
	}

	public void codeGen(CParseContext pctx) throws FatalErrorException {
		PrintStream o = pctx.getIOContext().getOutStream();
		o.println(";;; factor starts");
		if (factor != null) { factor.codeGen(pctx); }
		o.println(";;; factor completes");
	}
}