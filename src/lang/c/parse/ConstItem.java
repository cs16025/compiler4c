package lang.c.parse;

import lang.FatalErrorException;
import lang.c.*;

import java.io.PrintStream;

public class ConstItem extends Item {
    // constItem ::= [ MULT ] ident ASSIGN [ AMP ] num
    private CToken amp, num;

    public ConstItem(CParseContext pctx) {
        super(pctx);
        amp = null;
        num = null;
    }

    public static boolean isFirst(CToken tk) {
        return tk.getType() == CToken.TK_ASTERISK || Ident.isFirst(tk);
    }

    public void parse(CParseContext pctx) throws FatalErrorException {
        // ここにやってくるときは、必ずisFirst()が満たされている
        super.parse(pctx);


        CTokenizer ct;
        CToken tk;
        CSymbolTable st;

        ct = pctx.getTokenizer();
        tk = ct.getCurrentToken(pctx);
        st = pctx.getCSymbolTable();
        if (tk.getType() != CToken.TK_ASSIGN) {
            pctx.fatalError(tk.toExplainString() + "identの後はassignです");
        }
        tk = ct.getNextToken(pctx);
        if (tk.getType() == CToken.TK_AMP) {
            if (mult == null) {
                pctx.fatalError(tk.toExplainString() + "identがポインタ型ではありません");
            }
            amp = tk;
            tk = ct.getNextToken(pctx);
            if (Number.isFirst(tk)) {
                num = tk;
                tk = ct.getNextToken(pctx);
            } else {
                pctx.fatalError(tk.toExplainString() + "ampの後はnumです");
            }
        } else if (Number.isFirst(tk)) {
            if (mult != null) {
                pctx.fatalError(tk.toExplainString() + "identがポインタ型です");
            }
            num = tk;
            tk = ct.getNextToken(pctx);
        } else {
            pctx.fatalError(tk.toExplainString() + "assignの後はampかnumです");
        }
        if (mult != null && amp != null) {
            if(!st.setGlobalSymbol(ident.getText(), new CSymbolTableEntry(CType.getCType(CType.T_pint), num.getIntValue(), true, true, 0))) {
                pctx.fatalError(ident.toExplainString() + "重複宣言は禁止です");
            }
        } else {
            if(!st.setGlobalSymbol(ident.getText(), new CSymbolTableEntry(CType.getCType(CType.T_int), num.getIntValue(), true, true, 0))) {
                pctx.fatalError(ident.toExplainString() + "重複宣言は禁止です");
            }
        }
    }

    public void semanticCheck(CParseContext pctx) {}

    public void codeGen(CParseContext pctx) {
        PrintStream o = pctx.getIOContext().getOutStream();
        o.println(";;; constItem starts");
        if (ident != null) {
            o.println("" + ident.getText() + ": .WORD " + num.getIntValue() +  "\t; ConstItem: 定数用の領域を確保<" + ident.toExplainString() + ">");
        }
        o.println(";;; constItem completes");
    }
}
