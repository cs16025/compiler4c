package lang.c.parse;

import lang.FatalErrorException;
import lang.c.*;

import java.io.PrintStream;

public class DeclItem extends Item {
    // declItem ::= [ MULT ] ident [ LBRA num RBRA ]
    private CToken lbra, num;

    public DeclItem(CParseContext pctx) {
        super(pctx);
        ident = null;
        num = null;
    }

    public static boolean isFirst(CToken tk) {
        return tk.getType() == CToken.TK_ASTERISK || Ident.isFirst(tk);
    }

    public void parse(CParseContext pctx) throws FatalErrorException {
        // ここにやってくるときは、必ずisFirst()が満たされている
        super.parse(pctx);

        CTokenizer ct;
        CToken tk;
        CSymbolTable st;

        ct = pctx.getTokenizer();
        tk = ct.getCurrentToken(pctx);
        st = pctx.getCSymbolTable();
        if (tk.getType() == CToken.TK_LBRA) {
            lbra = tk;
            tk = ct.getNextToken(pctx);
            if (Number.isFirst(tk)) {
                num = tk;
                tk = ct.getNextToken(pctx);
            } else {
                pctx.fatalError(tk.toExplainString() + "lbraの後はnumです");
            }
            tk = ct.getCurrentToken(pctx);
            if (tk.getType() != CToken.TK_RBRA) {
                pctx.fatalError(tk.toExplainString() + "括弧が閉じていません");
            }
            tk = ct.getNextToken(pctx);
        }

        if (lbra != null) {
            if (mult != null) {
                if (!st.setGlobalSymbol(ident.getText(), new CSymbolTableEntry(CType.getCType(CType.T_parray), num.getIntValue(), false, true, 0))) {
                    pctx.fatalError(ident.toExplainString() + "重複宣言は禁止です");
                }
            } else {
                if(!st.setGlobalSymbol(ident.getText(), new CSymbolTableEntry(CType.getCType(CType.T_array), num.getIntValue(), false, true, 0))) {
                    pctx.fatalError(ident.toExplainString() + "重複宣言は禁止です");
                }
            }
        } else {
            if (mult != null) {
                if (!st.setGlobalSymbol(ident.getText(), new CSymbolTableEntry(CType.getCType(CType.T_pint), 1, false, true, 0))) {
                    pctx.fatalError(ident.toExplainString() + "重複宣言は禁止です");
                }
            } else {
                if(!st.setGlobalSymbol(ident.getText(), new CSymbolTableEntry(CType.getCType(CType.T_int), 1, false, true, 0))) {
                    pctx.fatalError(ident.toExplainString() + "重複宣言は禁止です");
                }
            }
        }
    }

    public void semanticCheck(CParseContext pctx) {}

    public void codeGen(CParseContext pctx) {
        PrintStream o = pctx.getIOContext().getOutStream();
        int type = pctx.getCSymbolTable().getGlobalSymbol(ident.getText()).getType().getType();
        o.println(";;; declItem starts");
        if (ident != null) {
            if (type == CType.T_int || type == CType.T_pint) {
                o.println("" + ident.getText() + ": .WORD 0" + "\t; DeclItem: 変数用の領域を確保<" + ident.toExplainString() + ">");
            }
            if (num != null) {
                o.println("" + ident.getText() + ": .BLKW " + num.getIntValue() + "\t; DeclItem: 配列用の領域を確保<" + ident.toExplainString() + ">");
            }
        }
        o.println(";;; declItem completes");
    }
}
