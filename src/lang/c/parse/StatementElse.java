package lang.c.parse;

import lang.FatalErrorException;
import lang.c.CParseContext;
import lang.c.CParseRule;
import lang.c.CToken;
import lang.c.CTokenizer;

import java.io.PrintStream;
import java.util.ArrayList;

public class StatementElse extends CParseRule {
    // statementElse ::= ELSE ( statementIf | LCUR { statement } RCUR )
    private CParseRule statementIf;
    private ArrayList<CParseRule> statements;
    private CToken tk_else, lcur;

    public StatementElse(CParseContext pctx) {
        statementIf = null;
        statements = new ArrayList<>();
        tk_else = null;
        lcur = null;
    }

    public static boolean isFirst(CToken tk) {
        return tk.getType() == CToken.TK_ELSE;
    }

    @Override
    public void parse(CParseContext pctx) throws FatalErrorException {
        // ここにやってくるときは、必ずisFirst()が満たされている
        CTokenizer ct;
        CToken tk;

        ct = pctx.getTokenizer();
        tk_else = ct.getCurrentToken(pctx);
        tk = ct.getNextToken(pctx);
        if (StatementIf.isFirst(tk)) {
            statementIf = new StatementIf(pctx);
            statementIf.parse(pctx);
        } else if (tk.getType() == CToken.TK_LCUR) {
            lcur = tk;
            tk = ct.getNextToken(pctx);
            while (Statement.isFirst(tk)) {
                CParseRule statement = new Statement(pctx);
                statement.parse(pctx);
                statements.add(statement);
                tk = ct.getCurrentToken(pctx);

            }
            if (tk.getType() == CToken.TK_RCUR) {
                tk = ct.getNextToken(pctx);
            } else {
                pctx.fatalError(lcur.toExplainString() + "括弧が閉じていません");
            }
        } else {
            pctx.fatalError(tk_else.toExplainString() + "elseの後はstatementIfElseかLCURです");
        }
    }

    @Override
    public void semanticCheck(CParseContext pctx) throws FatalErrorException {
        if (statementIf != null) {
            statementIf.semanticCheck(pctx);
        }
        for (CParseRule statement : statements) {
            statement.semanticCheck(pctx);
        }
    }

    @Override
    public void codeGen(CParseContext pctx) throws FatalErrorException {
        PrintStream o = pctx.getIOContext().getOutStream();
        o.println(";;; statementIfElse starts");
        if (statementIf != null) {
            statementIf.codeGen(pctx);
        }
        for (CParseRule statement : statements) {
            statement.codeGen(pctx);
        }
        o.println(";;; statementIfElse completes");
    }
}
