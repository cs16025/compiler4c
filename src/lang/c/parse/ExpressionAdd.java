package lang.c.parse;

import lang.FatalErrorException;
import lang.c.*;

import java.io.PrintStream;

class ExpressionAdd extends CParseRule {
    // expressionAdd ::= '+' term
    private CToken plus;
    private CParseRule left, right;

    public ExpressionAdd(CParseContext pctx, CParseRule left) {
        plus = null;
        this.left = left;
        right = null;
    }

    public static boolean isFirst(CToken tk) {
        return tk.getType() == CToken.TK_PLUS;
    }

    public void parse(CParseContext pctx) throws FatalErrorException {
        // ここにやってくるときは、必ずisFirst()が満たされている
        CTokenizer ct;
        CToken tk;

        ct = pctx.getTokenizer();
        plus = ct.getCurrentToken(pctx);
        // +の次の字句を読む
        tk = ct.getNextToken(pctx);
        if (Term.isFirst(tk)) {
            right = new Term(pctx);
            right.parse(pctx);
        } else {
            pctx.fatalError(tk.toExplainString() + "+の後ろはtermです");
        }
    }

    public void semanticCheck(CParseContext pctx) throws FatalErrorException {
        // 足し算の型計算規則
        final int s[][] = {
                //		T_err			T_int          T_pint       T_int_array     T_pint_array
                {	CType.T_err,	CType.T_err,    CType.T_err,    CType.T_err,    CType.T_err},   // T_err
                {	CType.T_err,	CType.T_int,    CType.T_pint,   CType.T_err,    CType.T_err},   // T_int
                {   CType.T_err,    CType.T_pint,   CType.T_err,    CType.T_err,    CType.T_err},   //T_pint
                {   CType.T_err,    CType.T_err,    CType.T_err,    CType.T_err,    CType.T_err},   //T_int_array
                {   CType.T_err,    CType.T_err,    CType.T_err,    CType.T_err,    CType.T_err},   //T_pint_array
        };
        int lt = 0;
        boolean lc = false;
        int rt = 0;
        boolean rc = false;
        int nt;

        if (left != null) {
            left.semanticCheck(pctx);
            lt = left.getCType().getType();		// +の左辺の型
            lc = left.isConstant();
        } else {
            pctx.fatalError(plus.toExplainString() + "左辺がありません");
        }

        if (right != null) {
            right.semanticCheck(pctx);
            rt = right.getCType().getType();	// +の右辺の型
            rc = right.isConstant();
        } else {
            pctx.fatalError(plus.toExplainString() + "右辺がありません");
        }

        nt = s[lt][rt];						// 規則による型計算
        if (nt == CType.T_err) {
            pctx.fatalError(plus.toExplainString() + "左辺の型[" + left.getCType().toString() + "]と右辺の型[" + right.getCType().toString() + "]は足せません");
        }
        this.setCType(CType.getCType(nt));
        this.setConstant(lc && rc);				// +の左右両方が定数のときだけ定数
    }

    public void codeGen(CParseContext pctx) throws FatalErrorException {
        PrintStream o = pctx.getIOContext().getOutStream();
        if ((left != null) && (right != null)) {
            left.codeGen(pctx);
            right.codeGen(pctx);
            o.println("\tMOV\t-(R6), R0\t; ExpressionAdd: ２数を取り出して、足し、積む<" + plus.toExplainString() + ">");
            o.println("\tADD\t-(R6), R0\t; ExpressionAdd:");
            o.println("\tMOV\tR0, (R6)+\t; ExpressionAdd:");
        }
    }
}
