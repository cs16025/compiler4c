package lang.c.parse;

import lang.FatalErrorException;
import lang.c.*;

import java.io.PrintStream;

public class ConditionGT extends CParseRule {
    // conditionGT ::= GT expression
    private CToken GT;
    private CParseRule left;
    private CParseRule right;

    public ConditionGT(CParseContext pctx, CParseRule left) {
        GT = null;
        this.left = left;
        right = null;
    }

    public static boolean isFirst(CToken tk) {
        return tk.getType() == CToken.TK_GT;
    }

    @Override
    public void parse(CParseContext pctx) throws FatalErrorException {
        // ここにやってくるときは、必ずisFirst()が満たされている
        CTokenizer ct;
        CToken tk;

        ct = pctx.getTokenizer();
        GT = ct.getCurrentToken(pctx);
        // >の次の字句を読む
        tk = ct.getNextToken(pctx);
        if (Expression.isFirst(tk)) {
            right = new Expression(pctx);
            right.parse(pctx);
        } else {
            pctx.fatalError(tk.toExplainString() + ">の後ろはexpressionです");
        }}

    @Override
    public void semanticCheck(CParseContext pctx) throws FatalErrorException {
        if (left != null && right != null) {
            left.semanticCheck(pctx);
            right.semanticCheck(pctx);
            if (!left.getCType().equals(right.getCType())) {
                pctx.fatalError(GT.toExplainString() + "左辺の型[" + left.getCType().toString() + "]と右辺の型[" + right.getCType().toString() + "]が一致しないので比較できません");
            } else {
                this.setCType(CType.getCType(CType.T_bool));
                this.setConstant(true);
            }
        }
    }

    @Override
    public void codeGen(CParseContext pctx) throws FatalErrorException {
        PrintStream o = pctx.getIOContext().getOutStream();
        o.println(";;; condition > (compare) starts");
        if (left != null && right != null) {
            left.codeGen(pctx);
            right.codeGen(pctx);
            int seq = pctx.getSeqId();
            o.println("\tMOV\t-(R6), R0\t; ConditionGT: ２数を取り出して、比べる");
            o.println("\tMOV\t-(R6), R1\t; ConditionGT:");
            o.println("\tMOV\t#0x0001, R2\t; ConditionGT: set true");
            o.println("\tCMP\tR1, R0\t; ConditionGT: R1>R0 = R0-R1<0");
            o.println("\tBRN\tGT" + seq + " ; ConditionGT:");
            o.println("\tCLR\tR2\t\t; ConditionGT: set false");
            o.println("GT" + seq + ":\tMOV\tR2, (R6)+\t; ConditionGT:");
        }
        o.println(";;;condition > (compare) completes");
    }
}
