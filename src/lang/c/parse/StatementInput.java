package lang.c.parse;

import lang.FatalErrorException;
import lang.c.CParseContext;
import lang.c.CParseRule;
import lang.c.CToken;
import lang.c.CTokenizer;

import java.io.PrintStream;

public class StatementInput extends CParseRule {
    // statementInput ::= INPUT (primary | factorAMP) SEMI
    private CToken input;
    private CParseRule porf;

    public StatementInput(CParseContext pctx) {
        input = null;
        porf = null;
    }

    public static boolean isFirst(CToken tk) {
        return tk.getType() == CToken.TK_INPUT;
    }

    @Override
    public void parse(CParseContext pctx) throws FatalErrorException {
        // ここにやってくるときは、必ずisFirst()が満たされている
        CTokenizer ct;
        CToken tk;

        ct = pctx.getTokenizer();
        input = ct.getCurrentToken(pctx);
        tk = ct.getNextToken(pctx);
        if (Primary.isFirst(tk)) {
            porf = new Primary(pctx);
            porf.parse(pctx);
        } else if (FactorAmp.isFirst(tk)) {
            porf = new FactorAmp(pctx);
            porf.parse(pctx);
        } else {
            pctx.fatalError(input.toExplainString() + "inputの後ろはprimaryかfactorAmpです");
        }
        tk = ct.getCurrentToken(pctx);
        if (tk.getType() == CToken.TK_SEMI) {
            tk = ct.getNextToken(pctx);
        } else {
            pctx.fatalError(tk.toExplainString() + "行末にゴミがついています");
        }
    }

    @Override
    public void semanticCheck(CParseContext pctx) throws FatalErrorException {
        if (porf != null) {
            porf.semanticCheck(pctx);
        }
    }

    @Override
    public void codeGen(CParseContext pctx) throws FatalErrorException {
        PrintStream o = pctx.getIOContext().getOutStream();
        o.println(";;; statementInput starts");
        if(porf != null) {
            porf.codeGen(pctx);
            o.println("\tMOV\t#0xFFE0, R1\t; StatementInput: 0xFFE0を用いて入力");
            o.println("\tMOV\t-(R6), R0\t; StatementInput:");
            o.println("\tMOV\t(R1), (R0)\t; StatementInput:");
        }
        o.println(";;; statementInput completes");
    }
}
