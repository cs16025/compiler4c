package lang.c.parse;

import lang.FatalErrorException;
import lang.c.*;

import java.io.PrintStream;

public class TermDiv extends CParseRule {
    //termDiv ::= DIV factor
    private CToken div;
    private CParseRule left, right;

    public TermDiv(CParseContext pctx, CParseRule left) {
        div = null;
        this.left = left;
        right = null;
    }

    public static boolean isFirst(CToken tk) {
        return tk.getType() == CToken.TK_SLASH;
    }

    public void parse(CParseContext pctx) throws FatalErrorException {
        // ここにやってくるときは、必ずisFirst()が満たされている
        CTokenizer ct;
        CToken tk;

        ct = pctx.getTokenizer();
        div = ct.getCurrentToken(pctx);
        // /の次の字句を読む
        tk = ct.getNextToken(pctx);
        if (Factor.isFirst(tk)) {
            right = new Factor(pctx);
            right.parse(pctx);
        } else {
            pctx.fatalError(tk.toExplainString() + "/の後ろはfactorです");
        }
    }

    public void semanticCheck(CParseContext pctx) throws FatalErrorException {
        //割り算の型計算規則
        final int s[][] = {
                //		T_err			T_int          T_pint       T_int_array     T_pint_array
                {	CType.T_err,	CType.T_err,    CType.T_err,    CType.T_err,    CType.T_err},	// T_err
                {	CType.T_err,	CType.T_int,    CType.T_err,    CType.T_err,    CType.T_err},	// T_int
                {   CType.T_err,    CType.T_err,    CType.T_err,    CType.T_err,    CType.T_err},   //T_pint
                {   CType.T_err,    CType.T_err,    CType.T_err,    CType.T_err,    CType.T_err},   //T_int_array
                {   CType.T_err,    CType.T_err,    CType.T_err,    CType.T_err,    CType.T_err},   //T_pint_array
        };
        int lt = 0;
        boolean lc = false;
        int rt = 0;
        boolean rc = false;
        int nt;

        if (left != null) {
            left.semanticCheck(pctx);
            lt = left.getCType().getType();     // /の左辺の型
            lc = left.isConstant();
        } else {
            pctx.fatalError(div.toExplainString() + "左辺がありません");
        }

        if (right != null) {
            right.semanticCheck(pctx);
            rt = right.getCType().getType();	// /の右辺の型
            rc = right.isConstant();
        } else {
            pctx.fatalError(div.toExplainString() + "右辺がありません");
        }

        nt = s[lt][rt];
        if (nt == CType.T_err) {
            pctx.fatalError(div.toExplainString() + "左辺の型[" + left.getCType().toString() + "]と右辺の型[" + right.getCType().toString() + "]は割れません");
        }
        this.setCType(CType.getCType(nt));
        this.setConstant(lc && rc);				// /の左右両方が定数のときだけ定数
    }

    public void codeGen(CParseContext pctx) throws FatalErrorException {
        PrintStream o = pctx.getIOContext().getOutStream();
        if ((left != null) && (right != null)) {
            left.codeGen(pctx);
            right.codeGen(pctx);
            o.println("\tJSR\tDIV\t; TermDiv: 割り算のサブルーチンに移動<" + div.toExplainString() + ">");
            o.println("\tSUB\t#2, R6\t; TermDiv");
            o.println("\tMOV\tR0, (R6)+\t; TermDiv");
        }

    }
}
