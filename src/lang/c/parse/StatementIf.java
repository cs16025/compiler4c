package lang.c.parse;

import lang.FatalErrorException;
import lang.c.CParseContext;
import lang.c.CParseRule;
import lang.c.CToken;
import lang.c.CTokenizer;

import java.io.PrintStream;
import java.util.ArrayList;

public class StatementIf extends CParseRule {
    // statementIf ::= IF LPAR condition RPAR LCUR  { statement } RCUR [ statementElse ]
    private CToken tk_if, lpar, rpar, lcur;
    private CParseRule condition, statementElse;
    private ArrayList<CParseRule> statements;

    public StatementIf(CParseContext pctx) {
        tk_if = null;
        lpar = null;
        rpar = null;
        lcur = null;
        condition = null;
        statementElse = null;
        statements = new ArrayList<>();
    }

    public static boolean isFirst(CToken tk) {
        return tk.getType() == CToken.TK_IF;
    }

    @Override
    public void parse(CParseContext pctx) throws FatalErrorException {
        // ここにやってくるときは、必ずisFirst()が満たされている
        CTokenizer ct;
        CToken tk;

        ct = pctx.getTokenizer();
        tk_if = ct.getCurrentToken(pctx);
        tk = ct.getNextToken(pctx);
        if (tk.getType() == CToken.TK_LPAR) {
            lpar = tk;
            tk = ct.getNextToken(pctx);
            if (Condition.isFirst(tk)) {
                condition = new Condition(pctx);
                condition.parse(pctx);
                tk = ct.getCurrentToken(pctx);
                if (tk.getType() == CToken.TK_RPAR) {
                    rpar = tk;
                    tk = ct.getNextToken(pctx);
                    if (tk.getType() == CToken.TK_LCUR) {
                        lcur = tk;
                        tk = ct.getNextToken(pctx);
                        while (Statement.isFirst(tk)) {
                            CParseRule statement = new Statement(pctx);
                            statement.parse(pctx);
                            statements.add(statement);
                            tk = ct.getCurrentToken(pctx);
                        }
                        if (tk.getType() == CToken.TK_RCUR) {
                            tk = ct.getNextToken(pctx);
                            if (StatementElse.isFirst(tk)) {
                                statementElse = new StatementElse(pctx);
                                statementElse.parse(pctx);
                            }
                        } else {
                            pctx.fatalError(lcur.toExplainString() + "括弧が閉じていません");
                        }
                    } else {
                        pctx.fatalError(rpar.toExplainString() + "RPARの後ろはLCURです");
                    }
                } else {
                    pctx.fatalError(lpar.toExplainString() + "括弧が閉じていません");
                }
            } else {
                pctx.fatalError(lpar.toExplainString() + "LPARの後ろはconditionです");
            }
        } else {
            pctx.fatalError(tk_if.toExplainString() + "ifの後ろはLPARです");
        }
    }

    @Override
    public void semanticCheck(CParseContext pctx) throws FatalErrorException {
        if (condition != null) {
            condition.semanticCheck(pctx);
        }
        for (CParseRule statement : statements) {
            statement.semanticCheck(pctx);
        }
        if (statementElse != null) {
            statementElse.semanticCheck(pctx);
        }
    }

    @Override
    public void codeGen(CParseContext pctx) throws FatalErrorException {
        PrintStream o = pctx.getIOContext().getOutStream();
        o.println(";;; statementIf starts");
        if (condition != null) {
            condition.codeGen(pctx);
            int seq1 = pctx.getSeqId();
            int seq2 = pctx.getSeqId();
            o.println("\tMOV\t-(R6), R0\t; StatementIf: フラグを取り出して、判定する<" + tk_if.toExplainString() + ">");
            o.println("\tBRZ\tELSE" + seq1 + "; StatementIf:");
            for (CParseRule statement : statements) {
                statement.codeGen(pctx);
            }
            o.println("\tJMP\tEND" + seq2 + "; StatementIf:");
            o.println("ELSE" + seq1 + ":\t; StatementIf:");
            if (statementElse != null) {
                statementElse.codeGen(pctx);
            }
            o.println("END" + seq2 + ":\t; StatementIf:");
        }
        o.println(";;; statementIf completes");
    }
}
