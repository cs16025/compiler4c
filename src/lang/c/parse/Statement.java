package lang.c.parse;

import lang.FatalErrorException;
import lang.ParseContext;
import lang.c.CParseContext;
import lang.c.CParseRule;
import lang.c.CToken;
import lang.c.CTokenizer;

import java.io.PrintStream;

public class Statement extends CParseRule {
    //statement ::= statementAssign | statementIf | statementElse | statementWhile | statementInput | StatementOutput
    private CParseRule statement;

    Statement(ParseContext pctx) {
        statement = null;
    }

    public static boolean isFirst(CToken tk) {
        return StatementAssign.isFirst(tk) || StatementIf.isFirst(tk) || StatementElse.isFirst(tk) || StatementWhile.isFirst(tk) || StatementInput.isFirst(tk) || StatementOutput.isFirst(tk);
    }

    public void parse(CParseContext pctx) throws FatalErrorException {
        // ここにやってくるときは、必ずisFirst()が満たされている
        CTokenizer ct;
        CToken tk;

        ct = pctx.getTokenizer();
        tk = ct.getCurrentToken(pctx);
        if (StatementAssign.isFirst(tk)) {
            statement = new StatementAssign(pctx);
            statement.parse(pctx);
        } else if (StatementIf.isFirst(tk)) {
            statement = new StatementIf(pctx);
            statement.parse(pctx);
        } else if (StatementElse.isFirst(tk)) {
            statement = new StatementElse(pctx);
            statement.parse(pctx);
        } else if (StatementWhile.isFirst(tk)) {
            statement = new StatementWhile(pctx);
            statement.parse(pctx);
        } else if (StatementInput.isFirst(tk)) {
            statement = new StatementInput(pctx);
            statement.parse(pctx);
        } else if (StatementOutput.isFirst(tk)) {
            statement = new StatementOutput(pctx);
            statement.parse(pctx);
        }
    }

    public void semanticCheck(CParseContext pctx) throws FatalErrorException {
        if (statement != null) {
            statement.semanticCheck(pctx);
        }
    }

    public void codeGen(CParseContext pctx) throws FatalErrorException {
        PrintStream o = pctx.getIOContext().getOutStream();
        o.println(";;; statement starts");
        if (statement != null) { statement.codeGen(pctx); }
        o.println(";;; statement completes");
    }
}
