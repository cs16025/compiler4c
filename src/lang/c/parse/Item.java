package lang.c.parse;

import lang.FatalErrorException;
import lang.c.*;

public class Item extends CParseRule {
    protected CToken mult, ident;

    public Item(CParseContext pctx) {
        mult = null;
        ident = null;
    }

    public static boolean isFirst(CToken tk) {
        return tk.getType() == CToken.TK_ASTERISK || Ident.isFirst(tk);
    }

    @Override
    public void parse(CParseContext pctx) throws FatalErrorException {
        CTokenizer ct;
        CToken tk;
        CSymbolTable st;

        ct = pctx.getTokenizer();
        tk = ct.getCurrentToken(pctx);
        st = pctx.getCSymbolTable();
        if (tk.getType() == CToken.TK_ASTERISK) {
            mult = tk;
            tk = ct.getNextToken(pctx);
            if (Ident.isFirst(tk)) {
                ident = tk;
                tk = ct.getNextToken(pctx);
            } else {
                pctx.fatalError(tk.toExplainString() + "multの後はidentです");
            }
        } else if (Ident.isFirst(tk)) {
            ident = tk;
            tk = ct.getNextToken(pctx);
        }
    }

    @Override
    public void semanticCheck(CParseContext pctx) throws FatalErrorException {}

    @Override
    public void codeGen(CParseContext pctx) throws FatalErrorException {}
}
