package lang.c.parse;

import lang.FatalErrorException;
import lang.c.CParseContext;
import lang.c.CParseRule;
import lang.c.CToken;
import lang.c.CTokenizer;

import java.io.PrintStream;

public class StatementOutput extends CParseRule {
    // statementOutput ::= OUTPUT expression SEMI
    private CToken output;
    private CParseRule expression;

    public StatementOutput(CParseContext pctx) {
        output = null;
        expression = null;
    }

    public static boolean isFirst(CToken tk) {
        return tk.getType() == CToken.TK_OUTPUT;
    }
    
    @Override
    public void parse(CParseContext pctx) throws FatalErrorException {
        CTokenizer ct;
        CToken tk;

        ct = pctx.getTokenizer();
        output = ct.getCurrentToken(pctx);
        tk = ct.getNextToken(pctx);
        if (Expression.isFirst(tk)) {
            expression = new Expression(pctx);
            expression.parse(pctx);
        } else {
            pctx.fatalError(output.toExplainString() + "outputの後ろはexpressionです");
        }
        tk = ct.getCurrentToken(pctx);
        if (tk.getType() == CToken.TK_SEMI) {
            tk = ct.getNextToken(pctx);
        } else {
            pctx.fatalError(tk.toExplainString() + "行末にゴミがついています");
        }
    }

    @Override
    public void semanticCheck(CParseContext pctx) throws FatalErrorException {
        if (expression != null) {
            expression.semanticCheck(pctx);
        }
    }

    @Override
    public void codeGen(CParseContext pctx) throws FatalErrorException {
        PrintStream o = pctx.getIOContext().getOutStream();
        o.println(";;; statementOutput starts");
        if(expression != null) {
            expression.codeGen(pctx);
            o.println("\tMOV\t#0xFFE0, R1\t; StatementOutput: 0xFFE0を用いて出力");
            o.println("\tMOV\t-(R6), (R1)\t; StatementOutput:");
        }
        o.println(";;; statementOutput completes");
    }
}
