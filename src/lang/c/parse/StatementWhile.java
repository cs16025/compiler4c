package lang.c.parse;

import lang.FatalErrorException;
import lang.c.CParseContext;
import lang.c.CParseRule;
import lang.c.CToken;
import lang.c.CTokenizer;

import java.io.PrintStream;
import java.util.ArrayList;

public class StatementWhile extends CParseRule {
    //statementWhile ::= WHILE LPAR condition RPAR LCUR { statement } RCUR
    private CToken tk_while, lpar, rpar, lcur;
    private CParseRule condition;
    private ArrayList<CParseRule> statements;

    public StatementWhile(CParseContext pctx) {
        tk_while = null;
        lpar = null;
        rpar = null;
        lcur = null;
        condition = null;
        statements = new ArrayList<>();
    }

    public static boolean isFirst(CToken tk) {
        return tk.getType() == CToken.TK_WHILE;
    }

    @Override
    public void parse(CParseContext pctx) throws FatalErrorException {
        // ここにやってくるときは、必ずisFirst()が満たされている
        CTokenizer ct;
        CToken tk;

        ct = pctx.getTokenizer();
        tk_while = ct.getCurrentToken(pctx);
        tk = ct.getNextToken(pctx);
        if (tk.getType() == CToken.TK_LPAR) {
            lpar = tk;
            tk = ct.getNextToken(pctx);
            if (Condition.isFirst(tk)) {
                condition = new Condition(pctx);
                condition.parse(pctx);
                tk = ct.getCurrentToken(pctx);
                if (tk.getType() == CToken.TK_RPAR) {
                    rpar = tk;
                    tk = ct.getNextToken(pctx);
                    if (tk.getType() == CToken.TK_LCUR) {
                        lcur = tk;
                        tk = ct.getNextToken(pctx);
                        while (Statement.isFirst(tk)) {
                            CParseRule statement = new Statement(pctx);
                            statement.parse(pctx);
                            statements.add(statement);
                            tk = ct.getCurrentToken(pctx);
                        }
                        if (tk.getType() == CToken.TK_RCUR) {
                            tk = ct.getNextToken(pctx);
                        } else {
                            pctx.fatalError(lcur.toExplainString() + "括弧が閉じていません");
                        }
                    } else {
                        pctx.fatalError(rpar.toExplainString() + "RPARの後ろはLCURです");
                    }
                } else {
                    pctx.fatalError(lpar.toExplainString() + "括弧が閉じていません");
                }
            } else {
                pctx.fatalError(lpar.toExplainString() + "LPARの後ろはconditionです");
            }
        } else {
            pctx.fatalError(tk_while.toExplainString() + "ifの後ろはLPARです");
        }
    }

    @Override
    public void semanticCheck(CParseContext pctx) throws FatalErrorException {
        if (condition != null) {
            condition.semanticCheck(pctx);
        }
        for (CParseRule statement : statements) {
            statement.semanticCheck(pctx);
        }
    }

    @Override
    public void codeGen(CParseContext pctx) throws FatalErrorException {
        PrintStream o = pctx.getIOContext().getOutStream();
        o.println(";;; statementWhile starts");
        if(condition != null) {
            int seq1 = pctx.getSeqId();
            int seq2 = pctx.getSeqId();
            o.println("WHILE" + seq1 + ":\t; StatementWhile:");
            condition.codeGen(pctx);
            o.println("\tMOV\t-(R6), R0\t; StatementWhile: フラグを取り出して、判定する<" + tk_while.toExplainString() + ">");
            o.println("\tBRZ\tELSE" + seq2 + "\t; StatementWhile:");
            for(CParseRule statement : statements) {
                statement.codeGen(pctx);
            }
            o.println("\tJMP\tWHILE"+ seq1 +"\t;StatementWhile: ");
            o.println("ELSE" + seq2 + ":\t; StatementWhile:");
        }
        o.println(";;; statementWhile completes");
    }
}
