package lang.c.parse;

import lang.FatalErrorException;
import lang.c.*;

import java.io.PrintStream;

public class Array extends CParseRule {
    //array ::= LBRA expression RBRA
    private CToken lbra, rbra;
    private CParseRule expression;

    public Array(CParseContext pctx) {
        lbra = null;
        rbra = null;
        expression = null;
    }

    public static boolean isFirst(CToken tk) {
        return tk.getType() == CToken.TK_LBRA;
    }

    public void parse(CParseContext pctx) throws FatalErrorException {
        // ここにやってくるときは、必ずisFirst()が満たされている
        CTokenizer ct;
        CToken tk;

        ct = pctx.getTokenizer();
        lbra = ct.getCurrentToken(pctx);
        tk = ct.getNextToken(pctx);
        if (Expression.isFirst(tk)) {
            expression = new Expression(pctx);
            expression.parse(pctx);
            tk = ct.getCurrentToken(pctx);
            if (tk.getType() == CToken.TK_RBRA) {
                rbra = tk;
                tk = ct.getNextToken(pctx);
            } else {
                pctx.fatalError(tk.toExplainString() + "括弧が閉じていません");
            }
        } else {
            pctx.fatalError(tk.toExplainString() + "[の後ろはexpressionです");
        }
    }

    public void semanticCheck(CParseContext pctx) throws FatalErrorException {
        CTokenizer ct;
        CToken tk;

        if (expression != null) {
            expression.semanticCheck(pctx);
            ct = pctx.getTokenizer();
            tk = ct.getCurrentToken(pctx);
            if (expression.getCType() == CType.getCType(CType.T_int)) {
                setCType(expression.getCType());
                setConstant(expression.isConstant());
            } else {
                pctx.fatalError(tk.toExplainString() + "arrayの中のexpressionは" + CType.getCType(CType.T_int) + "型です");
            }
        }
    }

    public void codeGen(CParseContext pctx) throws FatalErrorException {
        PrintStream o = pctx.getIOContext().getOutStream();
        o.println(";;; Array starts");
        if (expression != null) {
            expression.codeGen(pctx);
        }
        o.println("\tMOV\t-(R6), R1\t; Array: 数を取り出して、アドレスを取り出して、数をアドレスに足して、積む<" + lbra.toExplainString() + ">");
        o.println("\tMOV\t-(R6), R0\t; Array:");
        o.println("\tADD\tR1, R0\t; Array:");
        o.println("\tMOV\tR0, (R6)+\t; Array:");
        o.println(";;; Array completes");
    }
}
