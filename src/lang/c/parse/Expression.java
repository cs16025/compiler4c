package lang.c.parse;

import java.io.PrintStream;

import lang.*;
import lang.c.*;

public class Expression extends CParseRule {
	// expression ::= term { expressionAdd | expressionSub }
	private CParseRule expression;

	public Expression(CParseContext pctx) {
		expression = null;
	}

	public static boolean isFirst(CToken tk) {
		return Term.isFirst(tk);
	}

	public void parse(CParseContext pctx) throws FatalErrorException {
		// ここにやってくるときは、必ずisFirst()が満たされている
		CParseRule term;
		CTokenizer ct;
		CToken tk;
		CParseRule list;

		term = new Term(pctx);
		term.parse(pctx);

		ct = pctx.getTokenizer();
		tk = ct.getCurrentToken(pctx);

		while (ExpressionAdd.isFirst(tk) || ExpressionSub.isFirst(tk)) {
			if (ExpressionAdd.isFirst(tk)) {
				list = new ExpressionAdd(pctx, term);
				list.parse(pctx);
				term = list;
				tk = ct.getCurrentToken(pctx);
			} else if (ExpressionSub.isFirst(tk)) {
				list = new ExpressionSub(pctx, term);
				list.parse(pctx);
				term = list;
				tk = ct.getCurrentToken(pctx);
			}
		}
		expression = term;
	}

	public void semanticCheck(CParseContext pctx) throws FatalErrorException {
		if (expression != null) {
			expression.semanticCheck(pctx);
			this.setCType(expression.getCType());		// expression の型をそのままコピー
			this.setConstant(expression.isConstant());
		}
	}

	public void codeGen(CParseContext pctx) throws FatalErrorException {
		PrintStream o = pctx.getIOContext().getOutStream();
		o.println(";;; expression starts");
		if (expression != null) {
			expression.codeGen(pctx);
		}
		o.println(";;; expression completes");
	}
}
