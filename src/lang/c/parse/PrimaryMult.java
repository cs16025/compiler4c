package lang.c.parse;

import lang.FatalErrorException;
import lang.c.*;

import java.io.PrintStream;

public class PrimaryMult extends CParseRule{
    //primaryMult ::= MULT variable
    private CToken mult;
    private CParseRule variable;

    public PrimaryMult(CParseContext pctx) {
        mult = null;
        variable = null;
    }

    public static boolean isFirst(CToken tk) {
        return tk.getType() == CToken.TK_ASTERISK;
    }

    public void parse(CParseContext pctx) throws FatalErrorException {
        // ここにやってくるときは、必ずisFirst()が満たされている
        CTokenizer ct;
        CToken tk;

        ct = pctx.getTokenizer();
        mult = ct.getCurrentToken(pctx);
        tk = ct.getNextToken(pctx);
        if (Variable.isFirst(tk)) {
            variable = new Variable(pctx);
            variable.parse(pctx);
        } else {
            pctx.fatalError(tk.toExplainString() + "*の後ろはvariableです");
        }
    }

    public void semanticCheck(CParseContext pctx) throws FatalErrorException {
        CTokenizer ct;
        CToken tk;

        if (variable != null) {
            variable.semanticCheck(pctx);
            ct = pctx.getTokenizer();
            tk = ct.getCurrentToken(pctx);
            if (variable.getCType() == CType.getCType(CType.T_pint)) {
                setCType(CType.getCType(CType.T_int));
                setConstant(variable.isConstant());
            } else {
                pctx.fatalError(tk.toExplainString() + "variableは" + CType.getCType(CType.T_pint) + "型です");
            }
        }
    }

    public void codeGen(CParseContext pctx) throws FatalErrorException {
        PrintStream o = pctx.getIOContext().getOutStream();
        if (variable != null) {
            variable.codeGen(pctx);
            o.println("\tMOV\t-(R6), R0\t; PrimaryMult: アドレスを取り出して、内容を参照して、積む<" + mult.toExplainString() + ">");
            o.println("\tMOV\t(R0), (R6)+\t; PrimaryMult:");
        }
    }
}
