package lang.c.parse;

import lang.FatalErrorException;
import lang.c.*;

import java.io.PrintStream;

public class Condition extends CParseRule {
    // condition ::= expression ( conditionLT | conditionLE | conditionGT | conditionGE | conditionEQ | conditionNE ) | TRUE | FALSE
    private CParseRule expression;
    private CToken TorF;

    public Condition(CParseContext pctx) {
        expression = null;
        TorF = null;
    }

    public static boolean isFirst(CToken tk) {
        return Expression.isFirst(tk) || tk.getType() == CToken.TK_TRUE || tk.getType() == CToken.TK_FALSE;
    }

    @Override
    public void parse(CParseContext pctx) throws FatalErrorException {
        // ここにやってくるときは、必ずisFirst()が満たされている
        CTokenizer ct;
        CToken tk;
        CParseRule lterm, rterm = null;

        ct = pctx.getTokenizer();
        tk = ct.getCurrentToken(pctx);
        if (Expression.isFirst(tk)) {
            lterm = new Expression(pctx);
            lterm.parse(pctx);
            tk = ct.getCurrentToken(pctx);
            if (ConditionLT.isFirst(tk)) {
                rterm = new ConditionLT(pctx, lterm);
                rterm.parse(pctx);
            } else if (ConditionLE.isFirst(tk)) {
                rterm = new ConditionLE(pctx, lterm);
                rterm.parse(pctx);
            } else if (ConditionGT.isFirst(tk)) {
                rterm = new ConditionGT(pctx, lterm);
                rterm.parse(pctx);
            } else if (ConditionGE.isFirst(tk)) {
                rterm = new ConditionGE(pctx, lterm);
                rterm.parse(pctx);
            } else if (ConditionEQ.isFirst(tk)) {
                rterm = new ConditionEQ(pctx, lterm);
                rterm.parse(pctx);
            } else if (ConditionNE.isFirst(tk)) {
                rterm = new ConditionNE(pctx, lterm);
                rterm.parse(pctx);
            } else {
                pctx.fatalError(tk.toExplainString() + "expresstionの後は比較演算子です");
            }
            expression = rterm;
        } else if (tk.getType() == CToken.TK_TRUE || tk.getType() == CToken.TK_FALSE) {
            TorF = tk;
            tk = ct.getNextToken(pctx);
        }
    }

    @Override
    public void semanticCheck(CParseContext pctx) throws FatalErrorException {
        if (expression != null) {
            expression.semanticCheck(pctx);
        } else {
            if (TorF.getType() == CToken.TK_TRUE) {
                this.setCType(CType.getCType(CType.T_bool));
                this.setConstant(true);
            } else if (TorF.getType() == CToken.TK_FALSE) {
                this.setCType(CType.getCType(CType.T_bool));
                this.setConstant(true);
            }
        }
    }

    @Override
    public void codeGen(CParseContext pctx) throws FatalErrorException {
        PrintStream o = pctx.getIOContext().getOutStream();
        o.println(";;; condition starts");
        if (expression != null) {
            expression.codeGen(pctx);
        } else if (TorF != null) {
            if (TorF.getType() == CToken.TK_TRUE) {
                o.println("\tMOV\t#0x0001, R2\t; Condition: set true");
                o.println("\tMOV\tR2, (R6)+\t; Condition:");
            } else if (TorF.getType() == CToken.TK_FALSE) {
                o.println("\tCLR\tR2\t\t; Condition: set false");
                o.println("\tMOV\tR2, (R6)+\t; Condition:");
            }
        }
        o.println(";;; condition completes");
    }
}
