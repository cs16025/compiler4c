package lang.c.parse;

import lang.FatalErrorException;
import lang.ParseContext;
import lang.c.CParseContext;
import lang.c.CParseRule;
import lang.c.CToken;
import lang.c.CTokenizer;

import java.io.PrintStream;

public class StatementAssign extends CParseRule {
    //statementAssign ::= primary ASSIGN expression SEMI
    private CToken assign, semi;
    private CParseRule primary, expression;

    StatementAssign(ParseContext pctx) {
        assign = null;
        semi = null;
        primary = null;
        expression = null;
    }

    public static boolean isFirst(CToken tk) {
        return Primary.isFirst(tk);
    }

    public void parse(CParseContext pctx) throws FatalErrorException {
        // ここにやってくるときは、必ずisFirst()が満たされている
        CTokenizer ct;
        CToken tk;

        ct = pctx.getTokenizer();
        primary = new Primary(pctx);
        primary.parse(pctx);
        tk = ct.getCurrentToken(pctx);
        if (tk.getType() == CToken.TK_ASSIGN) {
            assign = tk;
            tk = ct.getNextToken(pctx);
        } else {
            pctx.fatalError(tk.toExplainString() + "primaryの後ろは=です");
        }
        if (Expression.isFirst(tk)) {
            expression = new Expression(pctx);
            expression.parse(pctx);
            tk = ct.getCurrentToken(pctx);
        } else {
            pctx.fatalError(tk.toExplainString() + "=の後ろはexpressionです");
        }
        if (tk.getType() == CToken.TK_SEMI) {
            semi = tk;
            tk = ct.getNextToken(pctx);
        } else {
            pctx.fatalError(tk.toExplainString() + "expressionの後ろは;です");
        }
    }

    public void semanticCheck(CParseContext pctx) throws FatalErrorException {
        int pt = -1, et = -1;
        if (primary != null) {
            primary.semanticCheck(pctx);
            pt = primary.getCType().getType();
        } else {
            pctx.fatalError(assign.toExplainString() + "左辺がありません");
        }
        if (expression != null) {
            expression.semanticCheck(pctx);
            et = expression.getCType().getType();
        } else {
            pctx.fatalError(assign.toExplainString() + "右辺がありません");
        }
        if (pt != et) {
            pctx.fatalError(assign.toExplainString() + "左辺の型[" + primary.getCType().toString() + "]と右辺の型[" + expression.getCType().toString() + "]が一致していません");
        }
        if (primary.isConstant()) {
            pctx.fatalError(assign.toExplainString() + "定数には代入できません");
        }
    }

    public void codeGen(CParseContext pctx) throws FatalErrorException {
        PrintStream o = pctx.getIOContext().getOutStream();
        o.println(";;; statementAssign starts");
        if (primary != null) {
            primary.codeGen(pctx);
        }
        if (expression != null) {
            expression.codeGen(pctx);
        }
        o.println("\tMOV\t-(R6), R1\t; StatementAssign: 数を取り出して、変数アドレスを取り出し、そのアドレスに値を積む<" + assign.toExplainString() + ">");
        o.println("\tMOV\t-(R6), R0\t; StatementAssign:");
        o.println("\tMOV\tR1, (R0)\t; StatementAssign:");
        o.println(";;; statementAssign completes");
    }
}
