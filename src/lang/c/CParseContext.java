package lang.c;

import lang.*;

public class CParseContext extends ParseContext {
	public CParseContext(IOContext ioCtx,  CTokenizer tknz, CSymbolTable cst) {
		super(ioCtx, tknz);
		setCSymbolTable(cst);
	}


	@Override
	public CTokenizer getTokenizer()		{ return (CTokenizer) super.getTokenizer(); }

	private int seqNo = 0;
    private CSymbolTable cst;

	public int getSeqId() {
	    return ++seqNo;
	}

	public void setCSymbolTable(CSymbolTable cst) {
		this.cst = cst;
	}

    public CSymbolTable getCSymbolTable() {
	    return cst;
    }
}
