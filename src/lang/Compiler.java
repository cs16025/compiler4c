package lang;


public interface Compiler<Pctx> {
	public abstract void semanticCheck(Pctx pctx) throws FatalErrorException;
	public abstract void codeGen(Pctx pctx) throws FatalErrorException;
}

