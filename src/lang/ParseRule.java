package lang;

public abstract class ParseRule<Pctx> {
	public abstract void parse(Pctx pctx) throws FatalErrorException;
	public abstract void semanticCheck(Pctx pctx) throws FatalErrorException;
	public abstract void codeGen(Pctx pctx) throws FatalErrorException;
}
